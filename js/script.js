/* --------------------------------------------------

	ui
	
-------------------------------------------------- */
var UI = (function ($) {
    var jk = {};
    jk.config = {
    	winW: $(window).outerWidth(),
    	winH: $(window).innerHeight(),
    	$viewport: $(".viewport"),
    	$_toggles: $(".target a.panel"),
    	jk_ui_elevator: ".jk_ui_elevator a",
    	menu: "a.js_menu"
    };
    jk.init = function () {
    	jk.window();
    	jk.viewport();
    	jk.ui.menu();
    	jk.ui.elevator();
    };
    jk.viewport = function () {
    	var h = jk.config.winH; 
    	
    	//$(".hero, *.touch .bg.fixed").css({"height" : h + "px"}); 
 
    };
    jk.window = function () {
    	var $window = $(window);
    	
    	$window.resize(function () {
    		jk.config.winH = $window.innerHeight();
			jk.viewport();
    	});
    }
  	jk.ui = {
  		menu : function() {
  			$(".js_menu").on("click", this, function(evt){
  				evt.preventDefault();	
  				console.log("here");
  				$(".js_nav").toggleClass("on");
  			});
  		},
  		elevator : function() {
  			var $_nav = $(jk.config.jk_ui_elevator);
  			
  			$_nav.on("click", this, function(e) {
  				history.pushState(null, e.target.textContent, e.target.href);
	  			e.preventDefault();
	  			var i = $(this).attr("href");
	  			jk.scrollPage(i, 60, 260);
	  			$_nav.removeClass("on");
	  			$(this).addClass("on");
  			});
  		}
  	};
  	jk.scrollPage = function (i, o, d) {
		var y = $(i).offset().top - o;
  		$('html, body').animate({
			scrollTop: y
		}, {duration: d, queue: false});
  	};
    return jk;
})(jQuery);
/* --------------------------------------------------

	Page
	1.	everything that needs to run when a page 
		is loaded up
	2.	Nav calls this after AJAX loading shtuff	
	
-------------------------------------------------- */
var Page = (function ($) {
	
    var jk = {};
    
    jk.init = function() {		
		FeederUI.init({
			start_url : "https://api.instagram.com/v1/tags/",
			tag : "firefoodmusic",
			client_id : "client_id=277fa06ca9f641249356582c90c9f401",
			target : ".js_target",
			pagination : ".js_paginate"
		});
		
		ScrollFX.init();
		UI.viewport();
		UI.window();
		UI.ui.elevator();    
    };
    
    return jk;
})(jQuery);
/*
	start it up
*/
// ################################################################################
$(function () {

	UI.ui.menu();
	Nav.init();
	Page.init();
	
});