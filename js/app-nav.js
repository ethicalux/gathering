/* --------------------------------------------------

	ui
	
-------------------------------------------------- */
var Nav = (function ($) {
    var jk = {};
    jk.config = {
    	viewport: $(".viewport"), // master container
    	content: $(".viewport .js_content"), // ajax content wrapper
    	modal: $(".js_modal"), // modal hook
    	nav: "html.history .js_nav", // ajax navigation
    	callBack: Page.init // Page load routine
    };
    jk.templates = {
    	pnf : "/404/"
    };
    jk.init = function () {
    	jk.historyAPI.init();
		jk.ui.nav.init(jk.config.nav);
    };
  	jk.ui = {
	  	nav : {
		  	init : function(nav) {
		  		var _nav =  $(nav);
		  		_nav.on("click", "a", function(evt) {
		  			if ($(this).hasClass("js_exempt")) {
		  			
		  				return;
		  			} else {
			  			evt.preventDefault();
			  			var _url = $(this).attr("href"),
			  				data = $(this).data(),
			  				modal = (data.target == "modal") ? true : false;
			  			
			  			jk.ui.loader.init();
			  			jk.ui.modal.close(false);	
			  			
			  			if (jk.getLoaded(_url, modal)) {
	
			  				jk.historyAPI.changeURL(_url, data.title, modal);
			  				jk.ui.loader.remove();
			  				$("nav[role='navigation']").removeClass("on");
			  				
			  			} else {
			  				jk.historyAPI.changeURL(jk.templates.pnf, data.title);
			  				
			  				// load 404 page?
			  				// jk.getLoaded(jk.templates.pnf);
			  			}
		  			}
		  			
		  			
		  		});
		  	},
	  		on : function(url) {
		  		$(jk.config.nav).find("a").removeClass("on");
	  			$.each($(jk.config.nav).find("a"), function(i){
		  			if ($(this).attr("href") == url) {
		  				$(this).addClass("on");
		  			}
	  			});
	  		}
	  	},
	  	modal : {
	  		init : function(html) {
				var data = {
					html : $(html).find(".js_content").html(),
					url : location.pathname
				}
				var modalBox = Mustache.to_html($("#modal").html(), data);
				$("body").append(modalBox);
				$("body").addClass("modal");
				jk.config.modal = $(".js_modal");
	  			
	  			var _close = jk.config.modal.find(".close");
	  			_close.on("click", this, function(evt) {
	  				evt.preventDefault();
	  				var url = $(this).attr("href");
	  				jk.config.modal.addClass("off");
	  				var timer = setTimeout(function() {
						jk.ui.modal.close(true, url);
					}, 300);
		  			
	  			});
	  		},
	  		close : function(goback, url) {
	  			
	  			jk.config.modal.remove();
				$("body").removeClass("modal");
	  			if (goback) history.replaceState( null , null, url );
	  			
	  		}
	  	},
	  	loader : {
	  		init : function() {
	  			$(".js_msg .wrapper").html("<div class='loading'></div>");
	  			$(".js_msg").addClass("on");
	  		},
	  		remove : function() {
	  			$(".js_msg .wrapper").html("");
	  			$(".js_msg").removeClass("on");
	  		}	
	  	}
  	};
  	jk.getLoaded = function(url, modal) {
  		var success = false;
  		
		$.ajaxSetup({ async: false, cache: false });
		$.ajax({
			url: url,
			dataType: 'html',
			success: function(html) {		
				var data  = $(html).find(".js_content").data();
				if (!modal) {
			    	jk.config.content.html($(html).find(".js_content"));
			    	jk.ui.nav.init(".viewport .js_nav");
			    	jk.ui.nav.on(url);
			    	$("html, body").animate({ scrollTop: 0 }, 10);
				} else {
					jk.ui.modal.init(html);
				}
				
		  		Page.init();
				//(jk.config.callBack) ? jk.config.callBack() : null;
				success = true;
			},
			error: function (err) {
				//console.log(err);
			}
		});
		
  		return success;
  		
  	};
  	jk.historyAPI = {
  		init : function () {
			function routine() {
				var url = location.pathname;
				jk.ui.modal.close(false);
				jk.ui.nav.on(url);
				if (location.hash) {
					var hash = location.hash.split("#modal=").pop();
					if (modal != "#modal") {	
						jk.getLoaded(modal, true);
					} else {
						return;
					}
				} else {
					jk.getLoaded(url, false);
				}
			}
			
			//run first time?
			//routine();
		
			setTimeout(function() {
				$(window).on("popstate", function(evt) {
					routine();
				});
			}, 1);
  		},
  		changeURL : function (url, title, modal) {
  			(modal) ? history.pushState(null, null, location.pathname + "#modal=" + url) : history.pushState(null, null, url);
  			document.title = title;
  		},
  		supports : function () {
  			return !!(window.history && history.pushState);
  		}
  	}
    return jk;
})(jQuery);